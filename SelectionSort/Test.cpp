using namespace std;
#include <iostream>
#include "SelectionSort.h"

int main(int argc, char *argv[])
{
    int *value = NULL;
    int num = 0;

    cout << "Input Nums:" << endl;
    cin >> num;

    value = new int[num];

    cout << "Input " << num << " Nums" << endl;
    for (int i = 0; i < num; i++)
    {
        cin >> value[i];
    }

    cout << "Before Sort" << endl;
    for (int i = 0; i < num; i++)
    {
        cout << value[i] << " ";
    }
    cout << endl;
    
    SelectionSort selectionSort;
    selectionSort.sort(Sort::ASCEND, value, num);

    cout << "After Sort" << endl;
    for (int i = 0; i < num; i++)
    {
        cout << value[i] << " ";
    }
    cout << endl;

    if (value != NULL)
    {
        delete[] value;
    }
    return 0;
}
