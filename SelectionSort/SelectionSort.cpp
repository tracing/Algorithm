#include "SelectionSort.h"

void SelectionSort::sort(SortType type, int *value, int num)
{
    int key = 0;
    for (int i = 0; i < num; i++)
    {
        int j = i + 1;
        int k = i;
        int tmp = 0;
        for (; j < num; j++)
        {
            if (compare(type, value[j], value[k]))
            {
                k = j;    
            }
        }
        tmp = value[i];
        value[i] = value[k];
        value[k] = tmp;
    }
}

