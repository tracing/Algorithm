#include "Sort.h"

class SelectionSort : public Sort
{
public:
    virtual void sort(SortType type, int *value, int num);  
};