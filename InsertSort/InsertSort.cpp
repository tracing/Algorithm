#include "InsertSort.h"


void InsertSort::sort(SortType type, int *value, int num)
{
    int key = 0;
    for (int i = 1; i < num; i++)
    {
        int j = i - 1;
        key = value[i];
        while (j >= 0 && compare(type, key, value[j]))
        {
            value[j + 1] = value[j];
            j--;
        }
        value[j + 1] = key;
    }
}

