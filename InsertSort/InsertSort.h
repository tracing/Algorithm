#include "Sort.h"

class InsertSort : public Sort
{
public:
    virtual void sort(SortType type, int *value, int num);
};