#include "Sort.h"

int Sort::compare(SortType type, int key, int value)
{
    if (type == ASCEND)
    {
        return key < value;
    }
    return key > value;
}