class Sort
{
public:
    enum SortType { ASCEND, DESCEND };
    virtual void sort(SortType type, int *value, int num) = 0;
    virtual int compare(SortType type, int key, int value);  
};